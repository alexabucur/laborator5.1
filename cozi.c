#include "l5.h"

typedef int Data;
typedef int INT_MIN;

struct Elem
{
    Data val;
    struct Elem* next;
};
typedef struct Elem Node;

struct Q
{
    Node *front, *rear
};
typedef struct Q Queue;


Queue* createQueue()
{
    Queue *q;
    q=(Queue *)malloc(sizeof(Queue));
    if (q==NULL) return NULL;
    q->front=q->rear=NULL;
    return q;
}

void enQueue(Queue*q, Data v)
{
    Node* newNode=(Node*)malloc(sizeof(Node));
    newNode->val=v;
    newNode->next=NULL;
    if (q->rear==NULL) q->rear=newNode;
    else
    {
        (q->rear)->next=newNode;
        (q->rear)=newNode;
    }
    if (q->front==NULL) q->front=q->rear;
}

Data deQueue(Queue*q)
{
    Node* aux;
    Data d;
    if (isEmptyQ(q))
        printf("INT_MIN");


    aux=q->front;
    d=aux->val;
    q->front=(q->front)->next;
    free(aux);
    return d;
}

int isEmptyQ(Queue*q)
{
    return (q->front==NULL);
}

void deleteQueue(Queue*q)
{
    Node* aux;
    while (!isEmptyQ(q))
    {
        aux=q->front;
        q->front=q->front->next;
        free(aux);
    }
    free(q);
}

void display(Queue*q)
{
    Node* aux;
    aux=q->front;
    printf("\n");
    while(aux!=NULL)
    {
        printf("%d\t", aux->val);
        aux=aux->next;
    }
}

void invert(Queue*q, Node ** top)
{
    Node* stackTop=NULL;
    for(int i=0; i<5; i++)
        push(&stackTop, deQueue(q));
    while(!isEmptyS(stackTop))
    {
        enQueue(q,pop(&stackTop));
    }
    display(q);
    printf("\n");
}
